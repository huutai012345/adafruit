$(document).ready(function () {
  $("#switchLight").click(function (e) {
    let isChecked = $("#switchLight").prop("checked");
    let value = "0";
    if (isChecked === true) {
      value = "1";
      $("body").css("background-color", "white");
    } else {
      $("body").css("background-color", "black");
    }

    createData(value);

    if (isChecked == false) {
      loadData();
    }
  });

  function createData(value) {
    $.ajax({
      type: "POST",
      url: "http://localhost:3000/api/v1/feed/data/",
      data: {
        value: value,
      },
      dataType: "json",
      success: function (response) {},
    });
  }

  function refreshData() {
    setTimeout(function () {
      $.ajax({
        url: "http://localhost:3000/api/v1/feed/data/last",
        type: "GET",
        dataType: "json",
        success: function (response) {
          if (response.value === "1") {
            $("#switchLight").prop("checked", true);
            $("body").css("background-color", "white");
          } else {
            $("#switchLight").prop("checked", false);
            $("body").css("background-color", "black");
          }
        },
        complete: refreshData,
      });
    }, 1000);
  }

  $("#mic").click(function (e) {
    $("#text").text("");
    listen();
  });

  function listen() {
    var recognition = new webkitSpeechRecognition();
    recognition.interimResults = true;
    recognition.lang = "vi-VN";
    recognition.onresult = function (event) {
      if (event.results.length > 0) {
        $("#text").text(event.results[0][0].transcript);
      }
    };
    recognition.onend = function (event) {
      postVoice($("#text").text());
    };
    recognition.start();
  }

  function postVoice(message) {
    if (message == "hello") {
      $("#text").text("hello there. world here!");
    } else if (message == "goodbye") {
      $("#text").text("bye bye");
    } else if (message == "bật đèn") {
      createData("1");
      $("#text").text("đã bật đèn");
    } else if (message == "Tắt Đèn") {
      createData("0");
      $("#text").text("đã tắt đèn");
    } else {
      $("#text").text(message);
    }
    say($("#text").text());
  }

  function loadData() {
    $("#data").empty();
    $.ajax({
      url: "http://localhost:3000/api/v1/feed/data/",
      type: "GET",
      dataType: "json",
      success: function (response) {
        response.forEach((elm, index) => {
          $(` <tr>
                <th>${index + 1}</th>
                <td>${elm.created_at}</td>
                <td>${elm.updated_at}</td>
                <td>${elm.used_time}</td>
                <td>${elm.power}</td>
              </tr>`).appendTo("#data");
        });
      },
    });
  }

  function say(text) {
    responsiveVoice.speak(text, "Vietnamese Female");
  }

  loadData();
  refreshData();
});
