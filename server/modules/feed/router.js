import express from "express";
import { createData, getAllData, getLastData } from "./handle.js";

const feedRouter = express.Router();

feedRouter.post("/data", createData);
feedRouter.get("/data", getAllData);
feedRouter.get("/data/last", getLastData);

export default feedRouter;
